//
//  ViewController.swift
//  RSATest
//
//  Created by easen on 2018/5/24.
//  Copyright © 2018年 easen. All rights reserved.
//
extension String {
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}
import UIKit
import SwiftyRSA
import Security

class ViewController: UIViewController,TokenDelegate{

    var tokenManager = getToken()
    var serverpublickey:PublicKey!
    var serverprivatekey:PrivateKey!
    var clinepublickey:PublicKey!
    var clineprivatekey:PrivateKey!
    var encodeclinekey:String = ""
    var decodeclinekey:String = ""
    var encodeasekey = ""
    var asekey = ""
    var serPubKey:PublicKey!
    var serverPublicKeyString = ""
    var EnAESKey = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        do {
            //產生公私鑰
            let clineKeyPair = try SwiftyRSA.generateRSAKeyPair(sizeInBits: 2048)
            clinepublickey = clineKeyPair.publicKey
            clineprivatekey = clineKeyPair.privateKey
            
        } catch  {
            
        }
        tokenManager.token1()
    }
    func getstring(s: String) {
        serverPublicKeyString = s
    }
    func getAESString(Data: String) {
        EnAESKey = Data
    }
    
    //把伺服器的key拿來加密自己的publickey
    @IBAction func firstTime(_ sender: Any) {
        //伺服器傳回來的publicKey
       let SPUBK = try! PublicKey(pemEncoded:serverPublicKeyString.fromBase64()!)
        //加密把Key轉PemString再轉成base64之後加密
        let message = try! ClearMessage(base64Encoded: clinepublickey.pemString().toBase64())
        let data = try! message.encrypted(with: SPUBK, padding: .PKCS1)
        //加密完的string回傳給伺服器
        print(data.base64String)
        tokenManager.token2(key: data.base64String)
    }
    
    @IBAction func secTime(_ sender: Any) {
        do {
            //從伺服器那得到的加密AESKey
            let EncryptedKey = EnAESKey
            
            
            let pencrypted = try EncryptedMessage(base64Encoded: EncryptedKey)
            //解密後的AESKey
            let AESKey = try pencrypted.decrypted(with: clineprivatekey, padding: .PKCS1)
            //base64AES
            let base64Key = AESKey.base64String
            //String
            let StringKey = try! AESKey.string(encoding: .utf8)
            //Data
            let DataKey = AESKey.data
        } catch  {
            print(error)
        }
        
    }

}

